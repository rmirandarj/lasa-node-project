module.exports = (app) => {
    const stores = require('../controllers/store.controller.js');

    // Create a new Store
    app.post('/stores', stores.create);

    // Retrieve all stores
    app.get('/stores', stores.findAll);

    // Retrieve a single Store with storeId
    app.get('/stores/:storeId', stores.findOne);

    // Update a Store with storeId
    app.put('/stores/:storeId', stores.update);

    // Delete a Store with storeId
    app.delete('/stores/:storeId', stores.delete);
}