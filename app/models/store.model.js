const mongoose = require('mongoose');

const StoreSchema = mongoose.Schema({
    codigoLoja: String,
    cnpj: String,
    inscricaoEstadual: String,
    qtdPDV: String,
    tipoPDV: String,
    logradouro: String,
    numero: String,
    complemento: String,
    bairro: String,
    cep: String,
    cadastroSoftbox: String,
    fusoHorario: String,
    nomeLoja: String,
    cadastroHydra: String,
    dataInauguracao: String,
    dataMigracao: String,
    uf: String,
    municipio: String,
    ddd: String,
    telefone: String,
    codigoIbgeUf: String,
    codigoIbgeMunicipio: String,
    dadosLegais1: String,
    dadosLegais2: String,
    voltagem: String,
    regiao: String,
    contatoCorporativo: String,
    codigoCentroDistribuicao: String,
    lojaHibrida: String,
    pis: String,
    cofins: String,
    cst: String,
    codigoOrigemIcms: String,
    cfopIcmS00: String,
    cfopOtherIcms: String
    
}, {
    timestamps: true
});

module.exports = mongoose.model('Store', StoreSchema);
