var csv = require("csvtojson");
const readStream=require('fs').createReadStream('./stores.csv');
const axios = require("axios");
let calls = [];

const postStoreToDb = (store) => {
  console.log("store: ", store);
  
 return axios.post("http://localhost:3000/stores", {
    ContentType : 'application/json',
    body : store
  })
}

csv()
  .fromStream(readStream)
  .subscribe((json,lineNumber)=>{

    try {
      
      postStoreToDb(getFormattedJsonStore(json))
          .then(r => {
              console.log('Stores saved successfully', r)
              return;
          })
          .catch(e => {
              console.log('Error on transaction', e)
              return;
          })
  
    } catch (err) {
        console.log(err);
        const message = `Error saving stores from csv to database.`;
        console.log(message);
        throw new Error(message);
    }

    console.log("executei")
    
  })

const getFormattedJsonStore = function (json){

  const store = {
    codigoLoja: String,
    cnpj: String,
    inscricaoEstadual: String,
    qtdPDV: String,
    tipoPDV: String,
    logradouro: String,
    numero: String,
    complemento: String,
    bairro: String,
    cep: String,
    cadastroSoftbox: String,
    fusoHorario: String,
    nomeLoja: String,
    cadastroHydra: String,
    dataInauguracao: String,
    dataMigracao: String,
    uf: String,
    municipio: String,
    ddd: String,
    telefone: String,
    codigoIbgeUf: String,
    codigoIbgeMunicipio: String,
    dadosLegais1: String,
    dadosLegais2: String,
    voltagem: String,
    regiao: String,
    contatoCorporativo: String,
    codigoCentroDistribuicao: String,
    lojaHibrida: String,
    pis: String,
    cofins: String,
    cst: String,
    codigoOrigemIcms: String,
    cfopIcmS00: String,
    cfopOtherIcms: String
  };

  let formatedStore = {};

  keys = Object.keys(store);
  
  values = Object.keys(json).map(function(key) {
    return json[key];
  });

  for(i = 0; i <  keys.length; i++){
    formatedStore[keys[i]] = values[i];
  }

  return formatedStore;
}


csv();