Documentação da API com informações referentes as lojas americanas

* Como rodar local? 

1. Ao clonar o projeto, executar ``npm install`` no console na pasta raiz do projeto.
2. Subir uma instancia do mongo na máquina
3. Executar o comando ``node server.js`` no terminal

* Endpoints? 

1. Para inserir novas informações no banco, dar ``post`` na url: ``/stores``
2. Para listar todas as informacoes que estão no banco, dar ``get`` na url: ``/stores``